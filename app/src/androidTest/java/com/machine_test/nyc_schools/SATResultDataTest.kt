package com.machine_test.nyc_schools

import com.machine_test.nyc_schools.data.apiresult.SATResultData
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class SATResultDataTest {
    private lateinit var satResultData: SATResultData

    @Before
    fun setUp() {
        satResultData = SATResultData(
            dbn = "123456",
            school_name = "Test High School",
            num_of_sat_test_takers = "100",
            sat_critical_reading_avg_score = "500",
            sat_math_avg_score = "550",
            sat_writing_avg_score = "520"
        )
    }

    @Test
    fun testSATResultDataProperties() {
        Assert.assertEquals("123456", satResultData.dbn)
        Assert.assertEquals("Test High School", satResultData.school_name)
        Assert.assertEquals("100", satResultData.num_of_sat_test_takers)
        Assert.assertEquals("500", satResultData.sat_critical_reading_avg_score)
        Assert.assertEquals("550", satResultData.sat_math_avg_score)
        Assert.assertEquals("520", satResultData.sat_writing_avg_score)
    }
}