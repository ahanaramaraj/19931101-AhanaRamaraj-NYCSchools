import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.machine_test.nyc_schools.data.local.Event
import com.machine_test.nyc_schools.data.local.EventObserver
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class EventTest {

    // Rule to execute LiveData-related operations synchronously
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var observer: Observer<Event<String>>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun getContentIfNotHandledshouldreturncontentifnothandled() {
        val event = Event("Test")
        val content = event.getContentIfNotHandled()
        assert(content == "Test")
    }

    @Test
    fun getContentIfNotHandledshouldreturn_null_if_already_handled() {
        val event = Event("Test")
        event.getContentIfNotHandled()
        val content = event.getContentIfNotHandled()
        assert(content == null)
    }

    @Test
    fun peekContent_should_return_content() {
        val event = Event("Test")
        val content = event.peekContent()
        assert(content == "Test")
    }

    @Test
    fun EventObserver_should_trigger_callback() {
        val event = Event("Test")
        val eventObserver = EventObserver<String> { value ->
            assert(value == "Test")
        }

        eventObserver.onChanged(event)
    }

    @Test
    fun EventObserverShould_not_trigger_callback_if_content_already_handled() {
        val event = Event("Test")
        val eventObserver = EventObserver<String> { value ->
            assert(value == "Test")
        }

        event.getContentIfNotHandled()
        eventObserver.onChanged(event)
    }

    @Test
    fun EventObserver_should_not_trigger_callback_if_content_is_null() {
        val event = Event(null)
        val eventObserver = EventObserver<String> { value ->
            assert(value == null)
        }
    }

}
