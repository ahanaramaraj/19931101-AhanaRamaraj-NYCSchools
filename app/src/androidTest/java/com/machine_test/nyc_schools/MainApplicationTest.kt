import com.machine_test.nyc_schools.MainApplication
import org.junit.Test

class MainApplicationTest {

    @Test
    fun testApplicationCreation() {
        // Create an instance of MainApplication
        val application = MainApplication()

        // Ensure that the application can be created without errors
        assert(application != null)
    }
}
