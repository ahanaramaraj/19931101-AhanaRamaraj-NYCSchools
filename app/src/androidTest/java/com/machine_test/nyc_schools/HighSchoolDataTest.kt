import com.machine_test.nyc_schools.data.apiresult.HighSchoolData
import com.machine_test.nyc_schools.data.apiresult.SATResultData
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class HighSchoolDataTest {
    private lateinit var highSchoolData: HighSchoolData

    @Before
    fun setUp() {
        highSchoolData = HighSchoolData(
            dbn = "123456",
            school_name = "Test High School",
            location = "Test Location",
            phone_number = "123-456-7890",
            school_email = "test@example.com",
            website = "http://www.example.com"
        )
    }

    @Test
    fun testHighSchoolDataProperties() {
        assertEquals("123456", highSchoolData.dbn)
        assertEquals("Test High School", highSchoolData.school_name)
        assertEquals("Test Location", highSchoolData.location)
        assertEquals("123-456-7890", highSchoolData.phone_number)
        assertEquals("test@example.com", highSchoolData.school_email)
        assertEquals("http://www.example.com", highSchoolData.website)
    }
}


