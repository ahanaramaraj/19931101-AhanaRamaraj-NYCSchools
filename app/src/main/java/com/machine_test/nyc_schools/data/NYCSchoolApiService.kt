package com.machine_test.nyc_schools.data

import com.machine_test.nyc_schools.data.apiresult.HighSchoolData
import com.machine_test.nyc_schools.data.apiresult.SATResultData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Url

interface NYCSchoolApiService {
    // 1.https://data.cityofnewyork.us/resource/s3k6-pzi2.json
    // 2. https://data.cityofnewyork.us/resource/f9bf-2cp4.json
    @GET("s3k6-pzi2.json")
    fun getNYCSchoolList(): Call<List<HighSchoolData>?>

    @GET
    fun getSATResult( @Url url: String): Call<List<SATResultData>?>

}