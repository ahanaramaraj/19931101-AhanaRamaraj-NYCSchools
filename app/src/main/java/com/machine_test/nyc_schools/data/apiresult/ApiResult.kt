package com.machine_test.nyc_schools.data.apiresult

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class HighSchoolData(
    var dbn: String? = null,
    var school_name: String? = null,
    var location: String? = null,
    var phone_number: String? = null,
    var school_email: String? = null,
    var website: String? = null
) : Parcelable

@Parcelize
data class SATResultData(
    var dbn: String? = null,
    var school_name: String? = null,
    var num_of_sat_test_takers: String? = null,
    var sat_critical_reading_avg_score: String? = null,
    var sat_math_avg_score: String? = null,
    var sat_writing_avg_score: String? = null
) : Parcelable

