package com.machine_test.nyc_schools

import com.machine_test.nyc_schools.di.component.DaggerAppComponent
import dagger.android.DaggerApplication

/**
 * This class is required for Dagger 2 configuration.
 * */

class MainApplication : DaggerApplication() {

    private val appComponent = DaggerAppComponent.factory().create(this)

    override fun applicationInjector() = appComponent
}




