package com.machine_test.nyc_schools.ui.main.sat_results

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import com.machine_test.nyc_schools.R
import com.machine_test.nyc_schools.databinding.FragmentSatResultsBinding
import com.machine_test.nyc_schools.ui.base.BaseFragment
import com.machine_test.nyc_schools.ui.utilz.checkInternetAvailable
import com.machine_test.nyc_schools.ui.utilz.snackBar
import com.machine_test.nyc_schools.view_model.SATResultViewModel

class SATResultFragment : BaseFragment() {
    private var _binding: FragmentSatResultsBinding? = null
    private val satResultViewModel: SATResultViewModel by injectActivityViewModels()
    private val binding get() = _binding!!
    private var args: SATResultFragmentArgs? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSatResultsBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@SATResultFragment
            viewModel = satResultViewModel
        }
        args = arguments?.let { SATResultFragmentArgs.fromBundle(it) }

        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    // Handle the back button event
                    findNavController().navigateUp()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getSATResultsForSelectedSchool("https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=${args?.schoolId!!}")
    }

    private fun getSATResultsForSelectedSchool(url: String) {
        satResultViewModel.isLoading.postValue(true)
        if (requireContext().checkInternetAvailable()) {
            satResultViewModel.getSATResults(url).observe(viewLifecycleOwner) {
                if (it?.isNotEmpty() == true) {
                    val satResults = it
                    binding.satResultData = satResults[0]
                    binding.isEmpty = satResults.isEmpty()
                } else {
                    activity.snackBar(getString(R.string.text_no_results_found))
                    binding.isEmpty = true
                }
                satResultViewModel.isLoading.postValue(false)
            }

        } else {
            requireActivity().snackBar(getString(R.string.check_internet))
            satResultViewModel.isLoading.postValue(false)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}