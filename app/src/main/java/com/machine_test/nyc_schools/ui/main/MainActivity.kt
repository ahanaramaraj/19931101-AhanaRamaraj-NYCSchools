package com.machine_test.nyc_schools.ui.main

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.machine_test.nyc_schools.R
import com.machine_test.nyc_schools.databinding.ActivityMainBinding
import com.machine_test.nyc_schools.ui.base.BaseActivity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class MainActivity : BaseActivity(), HasAndroidInjector {

    lateinit var binding: ActivityMainBinding

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Any>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return fragmentInjector
    }
}