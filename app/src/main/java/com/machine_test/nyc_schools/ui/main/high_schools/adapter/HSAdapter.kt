package com.machine_test.nyc_schools.ui.main.high_schools.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.machine_test.nyc_schools.data.apiresult.HighSchoolData
import com.machine_test.nyc_schools.databinding.AdapterHighSchoolsBinding
import com.machine_test.nyc_schools.view_model.HSViewModel


class HSAdapter(
    private var highSchoolList: List<HighSchoolData>? = null, private val hsViewModel: HSViewModel
) : RecyclerView.Adapter<HSAdapter.HSViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HSViewHolder {
        val binding =
            AdapterHighSchoolsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return HSViewHolder(binding)
    }

    override fun onBindViewHolder(holder: HSViewHolder, position: Int) {
        holder.bind(highSchoolList?.get(position)!!, hsViewModel)
    }

    override fun getItemCount(): Int {
        return highSchoolList!!.size
    }

    class HSViewHolder(
        private val binding: AdapterHighSchoolsBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(schoolData: HighSchoolData, hsViewModel: HSViewModel) {
            try {
                binding.schoolData = schoolData
                binding.viewModel = hsViewModel
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}
