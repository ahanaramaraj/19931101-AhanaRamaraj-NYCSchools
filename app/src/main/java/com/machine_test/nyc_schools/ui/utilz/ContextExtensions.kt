package com.machine_test.nyc_schools.ui.utilz

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.annotation.RequiresApi
import com.google.android.material.snackbar.Snackbar

/**
 * This extension method is required to display a Snackbar in an Activity.
 * */
fun Activity?.snackBar(message: String?) {
    try {
        message?.let {
            this?.let {
                Snackbar.make(
                    findViewById(android.R.id.content),
                    message,
                    Snackbar.LENGTH_LONG
                ).show()
            }
        }

    } catch (e: Exception) {
        e.printStackTrace()
    }
}

/**
 * This extension method is required to check internet availability in an Activity.
 * */
@RequiresApi(Build.VERSION_CODES.M)
fun Context.checkInternetAvailable(): Boolean {
    val connectivityManager =
        this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val capabilities =
        connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
    if (capabilities != null) {
        when {
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                return true
            }
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                return true
            }
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                return true
            }
        }
    }
    return false
}

