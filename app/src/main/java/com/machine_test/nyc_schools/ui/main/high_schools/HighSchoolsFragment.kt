package com.machine_test.nyc_schools.ui.main.high_schools

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.machine_test.nyc_schools.R
import com.machine_test.nyc_schools.data.local.EventObserver
import com.machine_test.nyc_schools.databinding.FragmentHighSchoolsBinding
import com.machine_test.nyc_schools.ui.base.BaseFragment
import com.machine_test.nyc_schools.ui.main.high_schools.adapter.HSAdapter
import com.machine_test.nyc_schools.ui.utilz.checkInternetAvailable
import com.machine_test.nyc_schools.ui.utilz.snackBar
import com.machine_test.nyc_schools.view_model.HSViewModel

class HighSchoolsFragment : BaseFragment() {
    private var _binding: FragmentHighSchoolsBinding? = null
    private val hsViewModel: HSViewModel by injectActivityViewModels()
    private val binding get() = _binding!!
    private var selectedSchoolID: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHighSchoolsBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@HighSchoolsFragment
            viewModel = hsViewModel
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getNYCHighSchools()
        observeData()
    }

    private fun getNYCHighSchools() {
        hsViewModel.isLoading.postValue(true)
        if (requireContext().checkInternetAvailable()) {
            hsViewModel.getNYCSchoolList().observe(viewLifecycleOwner) {
                if (it?.isNotEmpty() == true) {
                    val highSchoolList = it
                    binding.rcvTripList.layoutManager = LinearLayoutManager(requireContext())
                    binding.rcvTripList.adapter = HSAdapter(highSchoolList, hsViewModel)
                    binding.isEmpty = highSchoolList.isEmpty()
                } else {
                    activity.snackBar(it.toString())
                    binding.isEmpty = true
                }
                hsViewModel.isLoading.postValue(false)
            }

        } else {
            requireActivity().snackBar(getString(R.string.check_internet))
            hsViewModel.isLoading.postValue(false)
        }
    }

    private fun observeData() {
        hsViewModel.schoolId.observe(viewLifecycleOwner, Observer {
            selectedSchoolID = it
        })
        hsViewModel.navigateToSATResultFragment.observe(viewLifecycleOwner, EventObserver {
            if (findNavController().currentDestination?.id == R.id.highSchoolsFragment) {
                val action =
                    HighSchoolsFragmentDirections.actionHighSchoolsFragmentToSATResultFragment(
                        selectedSchoolID!!
                    )
                findNavController().navigate(action)
            }
        })
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}