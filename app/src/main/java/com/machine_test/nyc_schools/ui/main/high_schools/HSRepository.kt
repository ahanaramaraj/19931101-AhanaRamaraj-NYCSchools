package com.machine_test.nyc_schools.ui.main.high_schools

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import com.machine_test.nyc_schools.data.NYCSchoolApiService
import com.machine_test.nyc_schools.data.apiresult.HighSchoolData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@SuppressLint("LogNotTimber")
class HSRepository @Inject constructor(
    private val NYCSchoolApiService: NYCSchoolApiService
) {
    fun getNYCHighSchoolData(): MutableLiveData<List<HighSchoolData>?> {
        val liveData = MutableLiveData<List<HighSchoolData>?>()
        NYCSchoolApiService.getNYCSchoolList()
            .enqueue(object : Callback<List<HighSchoolData>?> {
                override fun onFailure(call: Call<List<HighSchoolData>?>, t: Throwable) {
                    liveData.postValue(null)
                }

                override fun onResponse(call: Call<List<HighSchoolData>?>, response: Response<List<HighSchoolData>?>) {
                    try {
                        if (response.isSuccessful) {
                            liveData.postValue(response.body()!!)
                        }
                    } catch (ex: Exception) {
                        liveData.postValue(null)
                    }
                }
            })
        return liveData
    }

}