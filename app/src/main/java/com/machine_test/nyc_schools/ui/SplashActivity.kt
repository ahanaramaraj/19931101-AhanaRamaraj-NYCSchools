package com.machine_test.nyc_schools.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.databinding.DataBindingUtil
import com.machine_test.nyc_schools.R
import com.machine_test.nyc_schools.databinding.ActivitySplashBinding
import com.machine_test.nyc_schools.ui.base.BaseActivity
import com.machine_test.nyc_schools.ui.main.MainActivity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import java.util.*
import javax.inject.Inject

@SuppressLint("CustomSplashScreen")
class SplashActivity : BaseActivity(), HasAndroidInjector {

    private lateinit var binding: ActivitySplashBinding

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Any>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_splash
        )
        launchNextScreen()
    }

    /**
     * This method is designed to handle splash screen to other screen navigate logic.
     * */
    fun launchNextScreen() {
        Handler(Looper.getMainLooper()).postDelayed({
            try {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }, 3000)
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return fragmentInjector
    }
}