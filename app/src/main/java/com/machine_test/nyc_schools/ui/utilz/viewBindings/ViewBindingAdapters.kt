package com.machine_test.nyc_schools.ui.utilz.viewBindings

import android.view.View
import androidx.databinding.BindingAdapter
/**
 * This extension method is required to handle adapter views visibility.
 * */
@BindingAdapter("goneUnless")
fun goneUnless(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}

