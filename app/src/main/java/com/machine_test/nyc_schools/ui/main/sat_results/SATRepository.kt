package com.machine_test.nyc_schools.ui.main.sat_results

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import com.machine_test.nyc_schools.data.NYCSchoolApiService
import com.machine_test.nyc_schools.data.apiresult.SATResultData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@SuppressLint("LogNotTimber")
class SATRepository @Inject constructor(
    private val NYCSchoolApiService: NYCSchoolApiService
) {
    fun getSATResults(url: String): MutableLiveData<List<SATResultData>?> {
        val liveData = MutableLiveData<List<SATResultData>?>()
        NYCSchoolApiService.getSATResult(url)
            .enqueue(object : Callback<List<SATResultData>?> {
                override fun onFailure(call: Call<List<SATResultData>?>, t: Throwable) {
                    liveData.postValue(null)
                }

                override fun onResponse(
                    call: Call<List<SATResultData>?>,
                    response: Response<List<SATResultData>?>
                ) {
                    try {
                        if (response.isSuccessful) {
                            liveData.postValue(response.body()!!)
                        }
                    } catch (ex: Exception) {
                        liveData.postValue(null)
                    }
                }
            })
        return liveData
    }

}