package com.machine_test.nyc_schools.di.component

import android.app.Application
import com.machine_test.nyc_schools.di.annotations.ApplicationScope
import com.machine_test.nyc_schools.di.modules.ActivityModule
import com.machine_test.nyc_schools.di.modules.AppModule
import com.machine_test.nyc_schools.di.modules.BaseModule
import com.machine_test.nyc_schools.di.modules.NetworkModule
import com.machine_test.nyc_schools.di.modules.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import javax.inject.Singleton

@ApplicationScope
@Singleton
@Component(
    modules = [AndroidInjectionModule::class,
        ActivityModule::class,
        BaseModule::class,
        ViewModelModule::class,
        AppModule::class,
        NetworkModule::class]
)

interface AppComponent : AndroidInjector<DaggerApplication> {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance application: Application): AppComponent
    }

    override fun inject(instance: DaggerApplication)
}