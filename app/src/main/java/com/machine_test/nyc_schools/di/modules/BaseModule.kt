package  com.machine_test.nyc_schools.di.modules

import com.machine_test.nyc_schools.ui.base.BaseActivity
import com.machine_test.nyc_schools.ui.base.BaseFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class BaseModule {

    @ContributesAndroidInjector
    internal abstract fun contributeViewModelActivity(): BaseActivity

    @ContributesAndroidInjector
    internal abstract fun contributeViewModelFragment(): BaseFragment
}
