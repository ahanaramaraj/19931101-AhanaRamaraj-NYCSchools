package com.machine_test.nyc_schools.di.modules.fragment


import com.machine_test.nyc_schools.di.annotations.FragmentScope
import com.machine_test.nyc_schools.ui.main.high_schools.HighSchoolsFragment
import com.machine_test.nyc_schools.ui.main.sat_results.SATResultFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeHighSchoolsFragment(): HighSchoolsFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeSATResultFragment(): SATResultFragment

}