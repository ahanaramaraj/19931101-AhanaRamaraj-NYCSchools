package com.machine_test.nyc_schools.di.modules

import android.content.ClipboardManager
import android.content.Context
import android.net.wifi.WifiManager
import com.machine_test.nyc_schools.MainApplication
import com.machine_test.nyc_schools.di.annotations.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @ApplicationScope
    @Provides
    fun provideContext(application: MainApplication): Context {
        return application.applicationContext
    }


    @Provides
    fun providesWifiManager(context: Context): WifiManager =
        context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager

    @Provides
    fun providesClipboardManager(context: Context): ClipboardManager =
        context.applicationContext.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
}