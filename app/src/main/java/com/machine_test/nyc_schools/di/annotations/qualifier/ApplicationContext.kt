package com.machine_test.nyc_schools.di.annotations.qualifier

import javax.inject.Qualifier

@Qualifier
annotation class ApplicationContext
