package  com.machine_test.nyc_schools.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.machine_test.nyc_schools.di.annotations.ViewModelKey
import com.machine_test.nyc_schools.di.factory.AppViewModelFactory
import com.machine_test.nyc_schools.view_model.HSViewModel
import com.machine_test.nyc_schools.view_model.SATResultViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(HSViewModel::class)
    abstract fun bindHomeViewModel(viewModel: HSViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SATResultViewModel::class)
    abstract fun bindSATResultViewModel(viewModel: SATResultViewModel): ViewModel

}

