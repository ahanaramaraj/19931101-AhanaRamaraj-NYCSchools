package com.machine_test.nyc_schools.di.modules

import com.machine_test.nyc_schools.ui.SplashActivity
import com.machine_test.nyc_schools.di.annotations.ActivityScope
import com.machine_test.nyc_schools.di.modules.fragment.MainActivityModule
import com.machine_test.nyc_schools.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityModule {

    @ActivityScope
    @ContributesAndroidInjector
    internal abstract fun contributeSplashActivity(): SplashActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    internal abstract fun contributeHomeActivity(): MainActivity

}





