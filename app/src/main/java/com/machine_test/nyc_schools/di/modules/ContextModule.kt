package  com.machine_test.nyc_schools.di.modules

import android.content.Context
import com.machine_test.nyc_schools.di.annotations.ApplicationScope
import com.machine_test.nyc_schools.di.annotations.qualifier.ApplicationContext
import dagger.Module
import dagger.Provides

@Module
class ContextModule(private val context: Context) {

    @Provides
    @ApplicationScope
    @ApplicationContext
    fun provideContext(): Context {
        return context
    }
}