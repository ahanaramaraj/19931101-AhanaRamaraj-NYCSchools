package com.machine_test.nyc_schools.view_model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.machine_test.nyc_schools.data.apiresult.SATResultData
import com.machine_test.nyc_schools.ui.main.sat_results.SATRepository
import javax.inject.Inject

class SATResultViewModel @Inject constructor(
    mApplication: Application,
    private val satRepository: SATRepository
) : AndroidViewModel(mApplication) {
    var isLoading = MutableLiveData<Boolean>()
    init {
        isLoading.postValue(false)
    }
    fun getSATResults(url: String): MutableLiveData<List<SATResultData>?> {
        return satRepository.getSATResults(url)
    }
}