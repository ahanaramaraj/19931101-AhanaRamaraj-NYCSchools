package com.machine_test.nyc_schools.view_model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.machine_test.nyc_schools.data.apiresult.HighSchoolData
import com.machine_test.nyc_schools.data.local.Event
import com.machine_test.nyc_schools.ui.main.high_schools.HSRepository
import javax.inject.Inject

class HSViewModel @Inject constructor(
    mApplication: Application, private val mHSRepository: HSRepository
) : AndroidViewModel(mApplication) {
    var isLoading = MutableLiveData<Boolean>()
    var schoolId = MutableLiveData<String>()
    val navigateToSATResultFragment = MutableLiveData<Event<Unit>>()

    init {
        isLoading.postValue(false)
        schoolId.postValue("")
    }

    fun getNYCSchoolList(): MutableLiveData<List<HighSchoolData>?> {
        return mHSRepository.getNYCHighSchoolData()
    }

    fun navigateToSATResultFragment(schoolId: String) {
        this.schoolId.postValue(schoolId)
        navigateToSATResultFragment.postValue(Event(Unit))
    }
}